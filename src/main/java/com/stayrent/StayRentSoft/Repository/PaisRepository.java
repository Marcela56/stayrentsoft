package com.stayrent.StayRentSoft.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stayrent.StayRentSoft.Model.Pais;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Integer>{
	
}
