package com.stayrent.StayRentSoft.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stayrent.StayRentSoft.Model.Naturaleza;

@Repository
public interface NaturalezaRepository extends JpaRepository<Naturaleza, Integer>{

}
