package com.stayrent.StayRentSoft.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.stayrent.StayRentSoft.Model.Ocupacion;

@Repository
public interface OcupacionRepository  extends JpaRepository<Ocupacion, Integer> {

}
