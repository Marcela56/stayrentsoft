package com.stayrent.StayRentSoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StayRentSoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(StayRentSoftApplication.class, args);
	}

}
