package com.stayrent.StayRentSoft.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stayrent.StayRentSoft.Model.Ocupacion;
import com.stayrent.StayRentSoft.Service.OcupacionService;

@RestController
@RequestMapping("/ocupacion")
public class OcupacionController {

	@Autowired
	private OcupacionService naturalezaService;
	
	@PostMapping(path="/insertar")
	public Ocupacion insertaOcup(@RequestBody Ocupacion natu) {
		return naturalezaService.save(natu); 
	}
	@PutMapping(path="/update/{id}")
	public Ocupacion updateOcup(@RequestBody Ocupacion natu,@PathVariable("id") int id) {
		natu.setF006_id(id);
		return naturalezaService.update(natu); 
	}
	@DeleteMapping(path="/delete/{Id}")
	public void deleteNOcup(@PathVariable Integer Id) {
		naturalezaService.delete(Id); 
	}
	@GetMapping(path="/all")
	public List<Ocupacion> listarOcup() {
		return naturalezaService.listar(); 
	}	
	@GetMapping(path = {"/{id}"})
    public Optional<Ocupacion> listarOcuptId(@PathVariable("id")int id){
        return naturalezaService.listarId(id);
    }

}
