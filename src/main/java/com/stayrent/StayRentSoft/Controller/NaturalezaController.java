package com.stayrent.StayRentSoft.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stayrent.StayRentSoft.Model.Naturaleza;
import com.stayrent.StayRentSoft.Service.NaturalezaService;

@RestController
@RequestMapping("/naturaleza")
public class NaturalezaController {

	@Autowired
	private NaturalezaService naturalezaService;
	
	@PostMapping(path="/insertar")
	public Naturaleza insertaNat(@RequestBody Naturaleza natu) {
		return naturalezaService.save(natu); 
	}
	@PutMapping(path="/update/{id}")
	public Naturaleza updateNat(@RequestBody Naturaleza natu,@PathVariable("id") int id) {
		natu.setF007_id(id);
		return naturalezaService.update(natu); 
	}
	@DeleteMapping(path="/delete/{Id}")
	public void deleteNat(@PathVariable Integer Id) {
		naturalezaService.delete(Id); 
	}
	@GetMapping(path="/all")
	public List<Naturaleza> listarNat() {
		return naturalezaService.listar(); 
	}	
	@GetMapping(path = {"/{id}"})
    public Optional<Naturaleza> listarNatId(@PathVariable("id")int id){
        return naturalezaService.listarId(id);
    }

}
