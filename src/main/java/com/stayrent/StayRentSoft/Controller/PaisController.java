package com.stayrent.StayRentSoft.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stayrent.StayRentSoft.Model.Pais;
import com.stayrent.StayRentSoft.Service.PaisService;

@RestController
@RequestMapping("/pais")
public class PaisController {

	@Autowired
	private PaisService paisService;
	
	@PostMapping(path="/insertar")
	public Pais insertarPais(@RequestBody Pais pais) {
		return paisService.save(pais); 
	}
	@PutMapping(path="/update/{id}")
	public Pais updatePais(@RequestBody Pais pais,@PathVariable("id") int id) {
		pais.setF001_id(id);
		return paisService.update(pais); 
	}
	@DeleteMapping(path="/delete/{Id}")
	public void deletePais(@PathVariable Integer Id) {
		paisService.delete(Id); 
	}
	@GetMapping(path="/all")
	public List<Pais> listarPais() {
		return paisService.listar(); 
	}	
	@GetMapping(path = {"/{id}"})
    public Optional<Pais> listarPaisId(@PathVariable("id")int id){
        return paisService.listarId(id);
    }

}
