package com.stayrent.StayRentSoft.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stayrent.StayRentSoft.Model.Ocupacion;
import com.stayrent.StayRentSoft.Repository.OcupacionRepository;

@Service
public class OcupacionService {

	@Autowired
	OcupacionRepository ocupacionRepo;
	
	public Ocupacion save(Ocupacion ti) {
		return ocupacionRepo.save(ti);
	}
	public Ocupacion update(Ocupacion t) {
		return ocupacionRepo.save(t);
	}
	public void delete(int id) {
		 ocupacionRepo.deleteById(id);
	}
	public List<Ocupacion> listar(){
		return ocupacionRepo.findAll();
	}	
	public Optional<Ocupacion> listarId(int id) {
		return ocupacionRepo.findById(id);
	}

}
