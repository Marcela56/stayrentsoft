package com.stayrent.StayRentSoft.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stayrent.StayRentSoft.Model.Pais;
import com.stayrent.StayRentSoft.Repository.PaisRepository;

@Service
public class PaisService {

	@Autowired
	PaisRepository paisRepo;
	
	public Pais save(Pais ti) {
		return paisRepo.save(ti);
	}
	public Pais update(Pais t) {
		return paisRepo.save(t);
	}
	public void delete(int id) {
		 paisRepo.deleteById(id);
	}
	public List<Pais> listar(){
		return paisRepo.findAll();
	}	
	public Optional<Pais> listarId(int id) {
		return paisRepo.findById(id);
	}

}
