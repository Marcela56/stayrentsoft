package com.stayrent.StayRentSoft.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stayrent.StayRentSoft.Model.Naturaleza;
import com.stayrent.StayRentSoft.Repository.NaturalezaRepository;

@Service
public class NaturalezaService {

	@Autowired
	NaturalezaRepository naturalezaRepo;
	
	public Naturaleza save(Naturaleza ti) {
		return naturalezaRepo.save(ti);
	}
	public Naturaleza update(Naturaleza t) {
		return naturalezaRepo.save(t);
	}
	public void delete(int id) {
		 naturalezaRepo.deleteById(id);
	}
	public List<Naturaleza> listar(){
		return naturalezaRepo.findAll();
	}	
	public Optional<Naturaleza> listarId(int id) {
		return naturalezaRepo.findById(id);
	}
}
