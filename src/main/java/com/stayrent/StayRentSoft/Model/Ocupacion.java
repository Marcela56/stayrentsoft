package com.stayrent.StayRentSoft.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t006_ocupacion")
public class Ocupacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int f006_id;
	@Column(name="f006_nombre")
	private String f006_nombre;
	
	public Ocupacion() {
		super();
	}		
	public Ocupacion(int idOcupacion, String nombreOcupacion) {
		super();
		this.f006_id = idOcupacion;
		this.f006_nombre = nombreOcupacion;
	}
	public int getF006_id() {
		return f006_id;
	}
	public void setF006_id(int f006_id) {
		this.f006_id = f006_id;
	}
	public String getF006_nombre() {
		return f006_nombre;
	}
	public void setF006_nombre(String f006_nombre) {
		this.f006_nombre = f006_nombre;
	}
	
}
