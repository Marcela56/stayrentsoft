package com.stayrent.StayRentSoft.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t001_pais")
public class Pais {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int f001_id;
	@Column(name="f001_nombre")
	private String f001_nombre;
	
	public Pais() {
		super();
	}		
	public Pais(int idPais, String nombrePais) {
		super();
		this.f001_id = idPais;
		this.f001_nombre = nombrePais;
	}
	public int getF001_id() {
		return f001_id;
	}
	public void setF001_id(int f001_id) {
		this.f001_id = f001_id;
	}
	public String getF001_nombre() {
		return f001_nombre;
	}
	public void setF001_nombre(String f001_nombre) {
		this.f001_nombre = f001_nombre;
	}
	
}
