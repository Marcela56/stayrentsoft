package com.stayrent.StayRentSoft.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t007_naturaleza")
public class Naturaleza {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int f007_id;
	@Column(name="f007_nombre")
	private String f007_nombre;
	
	public Naturaleza() {
		super();
	}		
	public Naturaleza(int idNaturaleza, String nombreNaturaleza) {
		super();
		this.f007_id = idNaturaleza;
		this.f007_nombre = nombreNaturaleza;
	}
	public int getF007_id() {
		return f007_id;
	}
	public void setF007_id(int f007_id) {
		this.f007_id = f007_id;
	}
	public String getF007_nombre() {
		return f007_nombre;
	}
	public void setF007_nombre(String f007_nombre) {
		this.f007_nombre = f007_nombre;
	}	
}
